app:
	docker-compose up

app-setup: development-setup-env app-build
	docker-compose run app bin/setup

app-build:
	docker-compose build

app-bash:
	docker-compose run app bash

development-setup-env:
	ansible-playbook ansible/development.yml -i ansible/development 

production-setup:
	ansible-playbook ansible/site.yml -i ansible/production -u ubuntu -vv

production-deploy:
	ansible-playbook ansible/deploy.yml -i ansible/production -u ubuntu -vv
